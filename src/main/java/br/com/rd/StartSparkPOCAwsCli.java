package br.com.rd;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduce;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceClientBuilder;
import com.amazonaws.services.elasticmapreduce.model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StartSparkPOCAwsCli {

    public static void main(String[] args) {
        if (args.length>0){
            criarCluster();
        }else {
            clusterDefault();
        }
    }

    public static void clusterDefault() {


        AWSCredentials credentials = new BasicAWSCredentials("xxx", "xxxxxxxxxxxxxxxxxxxxxxxxx");
//        AWSCredentialsProvider credentials = null;
//        try {
//            credentials = new ProfileCredentialsProvider("default");
//        } catch (Exception e) {
//            throw new AmazonClientException(
//                    "Cannot load credentials from .aws/credentials file. " +
//                            "Make sure that the credentials file exists and the profile name is specified within it.",
//                    e);
//        }


        AmazonElasticMapReduce emr = AmazonElasticMapReduceClientBuilder.defaultClient();
//                .standard()
//                .withRegion(Regions.US_EAST_1)
//                .withCredentials(new AWSStaticCredentialsProvider(credentials)).build();

        AddJobFlowStepsRequest req = new AddJobFlowStepsRequest()
                .withJobFlowId("j-3RD6EAKT7YX8S");


        List<StepConfig> stepConfigs = new ArrayList<>();

        HadoopJarStepConfig sparkStepConf = new HadoopJarStepConfig()
                .withJar("command-runner.jar")
                .withArgs("spark-submit",
                        "--master",
                        "spark://10.235.188.247:7077",
                        "--deploy-mode","cluster",
                        "--class","br.com.rd.StartSparkPOC",
                        "s3://s3-emr-univers-spark-dev/SparkPOC.jar");

        StepConfig sparkStep = new StepConfig()
                .withName("Spark Step")
                .withActionOnFailure("CONTINUE")
                .withHadoopJarStep(sparkStepConf);


        stepConfigs.add(sparkStep);

        req.withSteps(stepConfigs);

        AddJobFlowStepsResult result = emr.addJobFlowSteps(req);
        System.out.println("resultado:"+result.getStepIds());
        System.out.println("Valida o resultado com o comando abaixo:");
        System.out.println(String.format("sudo aws emr describe-step --cluster-id %s --step-id %s",
                req.getJobFlowId(),
                result.getStepIds()
                        .stream()
                        .map(Object::toString)
                        .collect(Collectors.joining(", "))));

    }


    public static void criarCluster() {


        //AWSCredentials credentials = new BasicAWSCredentials("xxx", "xxxxx");
        AWSCredentialsProvider credentials = null;
        try {
            credentials = new ProfileCredentialsProvider("default");
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load credentials from .aws/credentials file. " +
                            "Make sure that the credentials file exists and the profile name is specified within it.",
                    e);
        }


        AmazonElasticMapReduce emr = AmazonElasticMapReduceClientBuilder.standard()
                .withRegion(Regions.US_EAST_1)
                .withCredentials(credentials).build();

        // create a step to enable debugging in the AWS Management Console
        HadoopJarStepConfig sparkStepConf = new HadoopJarStepConfig()
                .withJar("command-runner.jar")
                .withArgs("spark-submit",
                        "--master",
                        "spark://10.235.188.247:7077",
                        "--deploy-mode","cluster",
                        "--class","br.com.rd.StartSparkPOC",
                        "s3://s3-emr-univers-spark-dev/SparkPOC.jar");

        StepConfig enabledebugging = new StepConfig()
                .withName("Enable debugging")
                .withActionOnFailure("CONTINUE")
                .withHadoopJarStep(sparkStepConf);


        // specify applications to be installed and configured when EMR creates the cluster
        Application hive = new Application().withName("Hive");
        Application spark = new Application().withName("Spark");
        Application ganglia = new Application().withName("Ganglia");
        Application zeppelin = new Application().withName("Zeppelin");

//        AddJobFlowStepsRequest req = new AddJobFlowStepsRequest()
//                .withJobFlowId("j-3RD6EAKT7YX8S");


        List<StepConfig> stepConfigs = new ArrayList<>();




        StepConfig sparkStep = new StepConfig()
                .withName("Spark Step")
                .withActionOnFailure("CONTINUE")
                .withHadoopJarStep(sparkStepConf);


        stepConfigs.add(sparkStep);



        RunJobFlowRequest request = new RunJobFlowRequest()
                .withName("Spark-POC")
                .withReleaseLabel("emr-6.8.0")
                .withSteps(enabledebugging)
                .withApplications(hive,spark,ganglia,zeppelin)
                .withServiceRole("EMR_DefaultRole")
                .withJobFlowRole("EMR_EC2_DefaultRole")
                .withLogUri("s3://s3-emr-univers-spark-dev/logs")
                .withInstances(new JobFlowInstancesConfig()
                        .withEc2KeyName("emr-univers-spark")
                        .withEc2SubnetId("subnet-0f7c6a30254412410")
                        .withInstanceCount(3)
                        .withKeepJobFlowAliveWhenNoSteps(true)
                        .withMasterInstanceType("m3.xlarge")
                        .withSlaveInstanceType("m3.xlarge"));


        RunJobFlowResult result = emr.runJobFlow(request);
        System.out.println("The cluster ID is " + result.toString());

        AddJobFlowStepsRequest req = new AddJobFlowStepsRequest()
                .withJobFlowId(result.getJobFlowId());


        req.withSteps(stepConfigs);

        AddJobFlowStepsResult resultSteps = emr.addJobFlowSteps(req);
        System.out.println("resultado:"+resultSteps.getStepIds());
        System.out.println("Valida o resultado com o comando abaixo:");
        System.out.println(String.format("sudo aws emr describe-step --cluster-id %s --step-id %s",
                result.getJobFlowId(),resultSteps.getStepIds().stream().map(Object::toString)
                .collect(Collectors.joining(", "))));


    }
}
